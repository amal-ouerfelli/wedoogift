import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Amount from "../components/Amount";
const Level3 = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      <Button
        variant="info"
        onClick={() => setShow(true)}
        style={{ width: 150 }}
      >
        <h4>Level 3</h4>
      </Button>
      {show && <Amount />}
    </>
  );
};
export default Level3;
