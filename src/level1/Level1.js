import React from "react";
import { Row } from "react-bootstrap";
import Card from "../components/Card";
import "./level1.css";

const Level1 = ({ cards, onClickCard }) => {
  return (
    <>
      {cards && cards.equal ? (
        <>
          <h6>Votre montant est composé des cartes suivantes:</h6>
          {cards.equal.cards.map((card, index) => {
            return <h5 key={index}>{card}</h5>;
          })}
        </>
      ) : (
        cards &&
        cards.ceil &&
        cards.floor && (
          <>
            <h6>Tu peux choisir une parmi les cartes suivantes :</h6>
            <Row className="row">
              <Card
                value={cards.ceil.value}
                color="#FFC808"
                onclick={onClickCard}
              />

              <Card
                value={cards.floor.value}
                color="black"
                textColor="#FFC808"
                onclick={onClickCard}
              />
            </Row>
          </>
        )
      )}
    </>
  );
};
export default Level1;
