import axios from "axios";

const ListCardsQuery = (data) => {
  return axios
    .get(
      `http://localhost:3000/shop/${data.shopId}/search-combination?amount=${data.amount}`,
      {
        headers: {
          authorization: "tokenTest123",
        },
      }
    )
    .then((response) => {
      return response.data;
    });
};

export { ListCardsQuery };
