import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import Level1 from "./level1/Level1";
import Level2 from "./level2/Level2";
import { Button, Row, Spinner } from "react-bootstrap";
import Amount from "./components/Amount";
import { useMutation } from "react-query";
import { ListCardsQuery } from "./queries/ListCards";
import { useEffect, useState } from "react";

function App() {
  const levels = [
    { name: "level 1", number: 1 },
    { name: "level 2", number: 2 },
  ];
  const mutation = useMutation(ListCardsQuery);
  const [amount, setAmount] = useState(0);
  const [selectedLevel, setSelectedLevel] = useState(0);
  const [cards, setCards] = useState();
  const [valid, setValid] = useState(true);

  const handleChange = ({ target }) => {
    setAmount(target.value);
  };
  const handleValidate = async () => {
    await mutation.mutate({ amount: amount, shopId: 5 });
  };
  useEffect(() => {
    setCards(mutation.data);
    if (
      mutation?.data?.ceil &&
      !mutation?.data?.floor &&
      !mutation?.data?.equal &&
      selectedLevel === 1
    ) {
      setAmount(mutation?.data?.ceil?.value);
    }
    if (
      !mutation?.data?.ceil &&
      mutation?.data?.floor &&
      !mutation?.data?.equal &&
      selectedLevel === 1
    ) {
      setAmount(mutation?.data?.floor?.value);
    }
    if (mutation?.data && !mutation?.data?.equal && selectedLevel === 2) {
      setValid(false);
    }
  }, [mutation.data, selectedLevel]);
  const onClickCard = (newAmount) => {
    setValid(true);
    setAmount(newAmount);
  };
  return (
    <div className="App">
      <Row className="row">
        {levels.map((level, index) => {
          return (
            <Button
              className="button yellow"
              onClick={() => setSelectedLevel(level.number)}
              key={index}
            >
              {level.name}
            </Button>
          );
        })}
      </Row>

      {mutation.isLoading && <Spinner animation="border" variant="warning" />}
      {selectedLevel > 0 && (
        <>
          <Amount
            handleValidate={handleValidate}
            handleChange={handleChange}
            amount={amount}
            valid={valid}
          />
          <>
            {cards && (
              <>
                {selectedLevel === 1 ? (
                  <Level1 cards={cards} onClickCard={onClickCard} />
                ) : (
                  <Level2
                    cards={cards}
                    onClickCard={onClickCard}
                    setValid={() => setValid(true)}
                  />
                )}
              </>
            )}
          </>
        </>
      )}
    </div>
  );
}

export default App;
