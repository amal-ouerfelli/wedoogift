import React, { useState } from "react";
import { Button, Row } from "react-bootstrap";
import "./level2.css";
const Level2 = ({ cards, onClickCard, setValid }) => {
  const [verif, setVerif] = useState(true);
  const onClickButton = (value) => {
    onClickCard(value);
    setValid();
    setVerif(false);
  };
  return (
    <>
      {cards && cards.equal ? (
        <>
          <h6>Votre montant est composé des cartes suivantes:</h6>
          {cards.equal.cards.map((card, index) => {
            return <h5 key={index}>{card}</h5>;
          })}
        </>
      ) : (
        <>
          {verif && (
            <Row className="row">
              {cards.floor && (
                <Button
                  variant="danger"
                  className="plusButton"
                  onClick={() => onClickButton(cards.floor.value)}
                >
                  -
                </Button>
              )}
              {cards.ceil && (
                <Button
                  variant="success"
                  className="plusButton"
                  onClick={() => onClickButton(cards.ceil.value)}
                >
                  <h1 style={{ textAlign: "center" }}>+</h1>
                </Button>
              )}
            </Row>
          )}
        </>
      )}
    </>
  );
};
export default Level2;
