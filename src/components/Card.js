import React from "react";

const Card = ({ value, color, textColor = "black", onclick }) => {
  return (
    <div
      style={{
        backgroundColor: color,
        width: 100,
        height: 100,
        borderRadius: 20,
        paddingTop: 20,
      }}
      onClick={() => onclick(value)}
    >
      <h1 style={{ textAlign: "center", color: textColor }}>{value}</h1>
    </div>
  );
};
export default Card;
