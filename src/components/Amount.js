import React from "react";
import { Button, Form } from "react-bootstrap";
import "../level1/level1.css";
const Amount = ({ handleChange, handleValidate, amount, valid }) => {
  return (
    <Form>
      <Form.Label className="formLabel">Amount :</Form.Label>
      <Form.Control
        name="Amount"
        type="number"
        placeholder="Amount "
        onChange={handleChange}
        value={amount}
        style={{ width: 200 }}
      />
      {valid && (
        <Button className="button blue margin" onClick={handleValidate}>
          Validate
        </Button>
      )}
    </Form>
  );
};
export default Amount;
